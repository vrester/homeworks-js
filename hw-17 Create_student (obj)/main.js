const fName = getName('Enter your name')
const lName = getName('Enter your surname')
const student = {
    firstName: fName,
    lastName: lName,
    createReportTable() {
        const reportTable = {}
        while (true) {
            let subject = prompt('Enter name of the subject')
            if (subject == null) {
                break
            }
            let mark = +prompt('Enter your mark')

            if (!subject.match(/^[a-zA-Z]+$/)) {
                alert('Incorrect name of the subject')
            } else if (isNaN(mark) || mark > 12) {
                alert('You entered NaN, possible marks is form 0 to 12')
            } else reportTable[subject] = mark
        }
        isEmpty() ? null : student.reportTable = reportTable
    },
    isPassed() {
        if (this.reportTable) {
            let badMarks = 0
            let avrMark = 0
            let count = 0
            for (let mark in this.reportTable) {
                avrMark += this.reportTable[mark]
                count++
                if (this.reportTable[mark] < 4) {
                    badMarks += 1
                }
            }
            badMarks === 0 ? alert('The student is transferred to the next course') : alert('The student does not transfer to the next course')
            avrMark / count > 7 ? alert('The student is awarded a scholarship') : alert('The student is not awarded a scholarship')
        }
    },
}

function isEmpty() {
    for (let key in student.reportTable) {
        return false
    }
    return true
}

function getName(question) {
    let name = prompt(`${question}`)
    while (!name || !name.match(/^[0-9a-zA-Z]+$/)) {
        alert('Incorrect name')
        name = prompt(`${question}`)
    }
    return name
}


student.isPassed()
debugger
student.createReportTable()
console.log(student)
student.isPassed()
console.log(student)