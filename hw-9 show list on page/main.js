const list = ['hello', ['Anton', 'Alifirenko'], 1, 2, 3,]
const test = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"]

function printList(arr, node = document.body) {
    const listFromArr = `${createListItems(arr)}`
    return node.insertAdjacentHTML('beforeend', listFromArr)
}

function createListItems(arr) {
    return `<ul>${arr.map(el => {
        return Array.isArray(el) ? createListItems(el) : `<li>${el}</li>`
    }).join(' ')}</ul>`
}

const button = document.querySelector('.btn')
button.addEventListener('click', () => {
    clearPage()
})

function clearPage(timer = 3) {
    const clear = setInterval(() => {
        if (timer < 1) {
            document.body.innerHTML = ''
            clearInterval(clear)
        } else button.textContent = timer
        timer--
    }, 1000)
}

printList(list)
printList(test)
