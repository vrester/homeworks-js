const tabs = document.querySelector('.tabs')
const tabsTitle = document.querySelectorAll('.tabs-title')
const tabsContent = document.querySelectorAll('.tabs-content')

tabs.addEventListener('click', e => {
    const target = e.target
    changeClassName(target)
})

function changeClassName(target) {
    if (!tabsTitle) return
    tabsTitle.forEach(element => element.classList.remove('active'))
    target.classList.add('active')

    if (tabsContent) {
        tabsContent.forEach(element => {
            element.classList.remove('active')
            if (element.dataset.name === target.dataset.name) {
                element.classList.add('active')
            }
        })
    }
}



