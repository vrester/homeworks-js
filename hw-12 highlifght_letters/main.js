// Чому для роботи з input не рекомендується використовувати клавіатуру?

/*Тому що таким чином буде некоректно відстежуватися заповнення Input за допомогою paste, voice і тп. */

let selectedButton;
document.body.addEventListener('keydown', e => {
    const key = e.key.toLowerCase()
    changeBtnStyle(key)
})

function changeBtnStyle(pressedKey) {
    // Шукаю чи є натиснута кнопка в html по data-name
    const pressedButton = document.querySelector(`[data-name = ${pressedKey}]`)
    if (!pressedButton) return
    // Якщо повторно нажали на кнопку, то не перефарбовуємо її, виходимо з функції
    if (pressedButton === selectedButton) return

    if (selectedButton) selectedButton.style.backgroundColor = 'black'
    // Записую останню натиснуту кнопку
    selectedButton = pressedButton
    //
    pressedButton.style.backgroundColor = 'blue'
}
