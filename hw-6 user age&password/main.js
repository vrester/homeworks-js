function createNewUser() {
    const newUser = {
        firstName: '',
        lastName: '',
        newFirstName(name) {
            Object.defineProperty(newUser, 'firstName', {
                value: name,
            });
        },
        newLastName(name) {
            Object.defineProperty(newUser, 'lastName', {
                value: name,
            });
        },
        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        getAge() {
            if (this.birthday) {
                const currentYear = new Date()
                if (this.birthday.getMonth() >= currentYear.getMonth() && this.birthday.getDay() > currentYear.getDay()) { // Перевірка на день та місяць народження
                    return `${this.firstName + ' ' + this.lastName + '\'s'} age is ${currentYear.getFullYear() - this.birthday.getFullYear() + 1}`
                }
                return `${this.firstName + ' ' + this.lastName + '\'s'} age is ${currentYear.getFullYear() - this.birthday.getFullYear()}`
            }
        },
        getPassword() {
            if (this.firstName && this.lastName && this.birthday) {
                return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.getFullYear()
            }
        }
    };
    Object.defineProperty(newUser, 'firstName', {
        writable: false,
        configurable: true,
        value: getUserName('enter your name'),
    })
    Object.defineProperty(newUser, 'lastName', {
        writable: false,
        configurable: true,
        value: getUserName('enter your surname'),
    })
    newUser.birthday = getDateOfBirth()
    return newUser;
}

function getUserName(question) {
    let fName = prompt(`${question}`);
    if (fName) {
        while (!fName || !fName.match(/^[0-9a-zA-Z]+$/)) {
            fName = prompt(`${question}`, [fName]).trim();
        }
        return fName;
    }
}

function getDateOfBirth() {
    let inputUserDate = prompt('Enter yours age in dd.mm.yyyy format')
    while (!/^[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{4}$/.test(inputUserDate)) {
        alert('Wrong date format')
        inputUserDate = prompt('Enter yours age in dd.mm.yyyy format', [inputUserDate])
    }
    const dateParts = inputUserDate.split('.') // Розбиваю введену строку на частини [dd,mm,yyyy]

    return new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);
}

let newUser = createNewUser()
console.log(newUser)
console.log(newUser.getAge())
console.log(newUser.getPassword())