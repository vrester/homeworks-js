const changeStyleBtn = document.querySelector('.nav-action-toggleTheme-btn')
const cssLink = document.querySelector('.css-link')
const mainTheme = 'css/style.css'
const bedTonesTheme = 'css/bed-tones-style.css'

document.addEventListener('DOMContentLoaded', () => {
    const key = localStorage.getItem('theme')
    if (key) loadCss(key)
})

changeStyleBtn.addEventListener('click', () => {
    setTheme(getAttr())
    localStorage.setItem('theme', getAttr())
})

function loadCss(theme) {
    theme === mainTheme ? setAttr(mainTheme) : setAttr(bedTonesTheme)
}

function setTheme(target) {
    target === mainTheme ? setAttr(bedTonesTheme) : setAttr(mainTheme)
}

function setAttr(attr) {
    return cssLink.setAttribute('href', attr)
}

function getAttr() {
    return cssLink.getAttribute('href')
}