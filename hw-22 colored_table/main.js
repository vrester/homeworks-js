const wrapper = document.querySelector('.wrapper')

function createTable() {
    const table = document.createElement('table')
    table.classList.add('table')
    const tdCreate = () => {
        const td = document.createElement('td')
        td.classList.add('td')
        return td
    }
    for (let i = 0; i < 30; i++) {
        const row = document.createElement('tr')
        for (let k = 0; k < 30; k++) {
            row.append(tdCreate())
        }
        table.append(row)
    }
    wrapper.append(table)
}

createTable()

const table = document.querySelector('table')

table.addEventListener('click', e => {
    if (e.target.classList.contains('td')) toggleClass(e.target, 'pressed')
})

document.body.addEventListener('click', e => {
    if (e.target === wrapper) {
        let pressedTd = document.querySelectorAll('.pressed')
        pressedTd.forEach(td => toggleClass(td, 'pressed'))
    }
})

function toggleClass(target, className) {
    target.classList.toggle(`${className}`)
}