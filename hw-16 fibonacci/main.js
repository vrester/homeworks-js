function fib(f0, f1, n) {
    if (n === 0) return f0
    else if (n === 1) return f1
    else if (n > 0) return fib(f0, f1, n - 2) + fib(f0, f1, n - 1)
    else if (n < 0) return fib(f0, f1, n + 2) - fib(f0, f1, n + 1)
}

function getNumber() {
    let inputNumber = +prompt('Enter number').trim()
    while (isNaN(inputNumber) || !Number.isInteger(inputNumber) || inputNumber > 20) { // Перевірка на введення цілого числа не > 20. Якщо вводити числа більше, то буде переповнювання стеку
        alert('The number does not meet the conditions')
        inputNumber = +prompt('Enter number', [inputNumber]).trim()
    }
    return inputNumber
}

// alert(fib(0, 1, getNumber()))
console.log(fib(0, 1, -10)) // -55
console.log(fib(0, 1, 10)) // 55
console.log(fib(0, 1, -5)) // 5
console.log(fib(0, 1, 5)) // 5
