function createNewUser() {
    const newUser = {
        firstName: '',
        lastName: '',
        newFirstName(name) {
            Object.defineProperty(newUser, 'firstName', {
                value: getUserName(name, null),
            })
        },
        newLastName(name) {
            Object.defineProperty(newUser, 'lastName', {
                value: getUserName(null, name),
            })
        },
        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase()
        }
    }
    Object.defineProperty(newUser, 'firstName', {
        writable: false,
        configurable: true,
        value: getUserName(true, null),
    })
    Object.defineProperty(newUser, 'lastName', {
        writable: false,
        configurable: true,
        value: getUserName(null, true),
    })
    return newUser
}

function getUserName(fName, lName) {
    if (fName) {
        while (!fName || !isNaN(fName) || !fName.match(/^[0-9a-zA-Z]+$/)) {
            fName = prompt('Enters your first name', [fName]).trim()
        }
        return fName
    }
    if (lName) {
        while (!lName || !isNaN(lName) || !lName.match(/^[0-9a-zA-Z]+$/)) {
            lName = prompt('Enter your last name ', [lName]).trim()
        }
        return lName
    }
}

let newUser = createNewUser()
console.log(newUser)
console.log(newUser.getLogin()) // тест метода getLogin()
newUser.firstName = 'TEST1'
newUser.lastName = 'TEST2'
console.log(newUser) // немає змін
newUser.newFirstName('Test1')
newUser.newLastName('Test2')
console.log(newUser) // Ім'я та прізвище змінились

//
// function createNewUser() {
//     const newUser = {
//         firstName: '',
//         lastName: '',
//         newFirstName(name) {
//             Object.defineProperty(newUser, 'firstName', {
//                 // value: getUserName(name, null),
//                 value: name,
//             });
//         },
//         newLastName(name) {
//             Object.defineProperty(newUser, 'lastName', {
//                 // value: getUserName(null, name),
//                 value: name,
//             });
//         },
//         getLogin() {
//             return (this.firstName[0] + this.lastName).toLowerCase();
//         },
//     };
//     Object.defineProperty(newUser, 'firstName', {
//         writable: false,
//         configurable: true,
//         value: getUserName('enter your name'),
//     });
//     Object.defineProperty(newUser, 'lastName', {
//         writable: false,
//         configurable: true,
//         value: getUserName('enter your surname'),
//     });
//     return newUser;
// }
//
// // function getUserName(fName, lName) {
// //     if (fName) {
// //         while (!fName || !isNaN(fName) || !fName.match(/^[0-9a-zA-Z]+$/)) {
// //             fName = prompt(‘Enter your first name’, [fName]).trim()
// //         }
// //         return fName
// //     }
// //     if (lName) {
// //         while (!lName || !isNaN(lName) || !lName.match(/^[0-9a-zA-Z]+$/)) {
// //             lName = prompt(‘Enter your last name ‘, [lName]).trim()
// //         }
// //         return lName
// //     }
// // }
// function getUserName(question) {
//     fName = prompt(`${question}`);
//     if (fName) {
//         while (!fName || !isNaN(fName) || !fName.match(/^[0-9a-zA-Z]+$/)) {
//             fName = prompt(`${question}`, [fName]).trim();
//         }
//         return fName;
//     }
//     // if (lName) {
//     //     while (!lName || !isNaN(lName) || !lName.match(/^[0-9a-zA-Z]+$/)) {
//     //         lName = prompt(‘Enter your last name ’, [lName]).trim()
//     //     }
//     //     return lName
//     // }
// }
//
// let newUser = createNewUser()
// console.log(newUser)
// console.log(newUser.getLogin()) // тест метода getLogin()
// newUser.firstName = 'TEST1'
// newUser.lastName = 'TEST2'
// console.log(newUser) // немає змін
// newUser.newFirstName('TEST1')
// newUser.newLastName('TEST2')
// console.log(newUser) // Ім’я та прізвище змінились