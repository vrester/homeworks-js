let deepClone = obj => {
    if (obj == null || typeof obj !== 'object') {
        return obj
    }
    let objClone = Array.isArray(obj) ? [] : {}
    for (let key in obj) {
        let value = obj[key]
        objClone[key] = deepClone(value)
    }
    return objClone
}

// Перевірка глибокого копіювання об'єкту
let obj = {
    a: 1,
    b: 2,
    math() {
        return console.log(this.a + this.b)
    },
    student: {
        name: 'Anton',
        tabel: {history: 4, math: 5}
    },
    array: [1, 2, 3, 4, 5, 6],
}
console.log(obj)
let objCopy = deepClone(obj)
console.log(objCopy)
console.log(`Об'єкт obj === об'єкту objCopy? ${obj === objCopy}`) // false

// Перевірка глибокого копіювання масиву
let arr = [1, 2, 3, [obj]]
console.log(arr)
let arrClone = deepClone(arr)
console.log(arrClone)
console.log(`Масив arr === масиву arrClone? ${arr === arrClone}`) // false
