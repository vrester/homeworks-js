function deadlineCalc(storyPoints, backlog, deadline) {
    const today = new Date()
    if (deadline < today) return console.log(`Недопустимий deadline, ${deadline.toLocaleDateString()} вже наступило`)
    // загальна кількість сторі поінтів команди
    const teamPerformance = storyPoints.reduce((acc, currentValue) => acc + currentValue)
    // загальна складність завдань
    const tasksScope = backlog.reduce((acc, currentValue) => acc + currentValue)
    // днів на виконання всіх завдань
    let daysToFinishTasks = Math.ceil((tasksScope / teamPerformance))
    const businessDaysToDeadline = getBusinessDaysToDeadline(deadline, today)

    if (businessDaysToDeadline >= daysToFinishTasks) {
        return console.log(`Усі завдання будуть успішно виконані за ${businessDaysToDeadline - daysToFinishTasks} днів до настання дедлайну!`)
    } else if (businessDaysToDeadline < daysToFinishTasks) {
        return console.log(`Команді розробників доведеться витратити додатково ${(daysToFinishTasks * 8) - (businessDaysToDeadline * 8)} годин після дедлайну, щоб виконати всі завдання в беклозі`)
    }
}

function getBusinessDaysToDeadline(endDay, startDay) {
    let msInDay = 86400000
    let days = Math.ceil((endDay - startDay) / msInDay)

    // Рахуємо скільки тижнів до дедлайну
    const totalWeeks = Math.floor(days / 7)
    // За кожен тиждень віднімаємо 2 робочих дні
    days -= (totalWeeks * 2)

    if (startDay.getDay() - endDay.getDay() > 1) {
        days -= 2
    }
    // Якщо старт припав на неділю, а дедлайн до суботи
    if (startDay.getDay() === 0 && endDay.getDay() !== 6) {
        days -= 1
    }
    // Якщо дедлайн субота, а початок після неділі
    if (endDay.getDay() === 6 && startDay !== 0) {
        days -= 1
    }
    // Якщо робочих днів 0, а сьогодні від пн до пт не раніше 8 години ранку і не пізніше 10 години ранку, то додаємо цей день як робочий
    if (startDay.getDay() > 0 && startDay.getDay() < 6 && days === 0 && startDay.getHours() >= 8 && startDay.getHours() <= 10) days += 1

    return days

}

deadlineCalc([3, 4, 1], [3, 3, 1, 4, 5, 3, 5], new Date(2022, 11, 26, 22))




