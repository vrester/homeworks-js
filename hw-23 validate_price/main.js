const input = document.querySelector('.input')
const wrapper = document.querySelector('.wrapper')
input.onfocus = function () {
    input.style.border = '2px solid green'
}
input.onblur = function () {
    removeSpan()
    if (input.value < 0) {
        input.style.border = '2px solid red'
        input.style.color = 'black'
        let spanError = createSpan(`Please enter correct price`)
        return wrapper.append(spanError)
    }
    const spanPrice = createSpan(`Поточна ціна: $${input.value}`)
    const removeButton = createButton()
    wrapper.prepend(spanPrice)
    document.querySelector('span').append(removeButton)
    input.style.border = 'none'
    input.style.color = 'green'
}

function removeSpan() {
    if (document.querySelector('span')) document.querySelector('span').remove()
}

function createSpan(message) {
    const span = document.createElement('span')
    span.textContent = message
    span.style.margin = '10px 0 10px 0'
    span.style.padding = '5px 10px'
    span.style.border = '1px solid black'
    span.style.borderRadius = '5px'
    span.style.display = 'flex'
    span.style.transition = 'all .5s ease'
    return span
}

function createButton() {
    const button = document.createElement('button')
    button.classList.add('remove')
    button.textContent = 'x'
    button.style.backgroundColor = '#e1cee4'
    button.style.border = '1px solid black'
    button.style.borderRadius = '50%'
    button.style.cursor = 'pointer'
    button.style.marginLeft = '10px'
    return button
}

wrapper.addEventListener('click', e => {
    if (e.target.classList.contains('remove')) {
        removeSpan()
        input.value = ''
    }
})