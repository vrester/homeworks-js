const images = document.querySelectorAll('.image-to-show')
const button = document.querySelector('.btn')
const timerPlaceholder = document.querySelector('.timer')

let imgId = 0
let secondsLeft = 300 // 3 seconds
// Set interval containers
let imageCounter = false
let timeCounter = false

function toggleImage(startFlag = false) {
    images[imgId].classList.remove('show')
    // Якщо викликаю функцію без аргументу startFlag, то просто відтворити першу картинку.
    // Коли я буду викликати функцію з аргументом по кліку, то запускати counter і змінювати картинки
    if (startFlag) imgId += 1
    if (imgId === images.length) imgId = 0
    images[imgId].classList.add('show')
}

function timer() {
    if (secondsLeft <= 0) {
        secondsLeft = 300 // 3 seconds
    }
    secondsLeft--;
    timerPlaceholder.textContent = (secondsLeft / 100).toFixed(1) + " seconds"
}

button.addEventListener('click', () => {
    // Перевірка чи був вже запущений лічильник
    if (imageCounter) {
        clearInterval(imageCounter)
        clearInterval(timeCounter)
        button.textContent = 'START'
        button.classList.remove('move')
        imageCounter = false
        return
    }
    imageCounter = setInterval(toggleImage, 3000, true)
    timeCounter = setInterval(timer, 10);
    button.classList.add('move')
    button.textContent = 'STOP'

})
toggleImage() //Ставлю першу картинку без затримки

