const btns = document.querySelectorAll('.btn')
const items = document.querySelectorAll('.list-item')
const markers = document.querySelectorAll('.marker')
let currentSlide = 1
const slidesLength = items.length


btns.forEach(btn => btn.addEventListener('click', e => {
    if (e.target.dataset.name === 'btn-prev') {
        currentSlide--
        if (currentSlide < 1) currentSlide = slidesLength
        changeSlide()
    } else {
        currentSlide++
        if (currentSlide > slidesLength) currentSlide = 1
        changeSlide()
    }
}))

function changeSlide() {
    items.forEach(item => {
        item.classList.remove('active')
        if (item.dataset.id === currentSlide.toString()) item.classList.add('active')
    })
    if (markers) {
        markers.forEach(marker => {
            marker.classList.remove('active')
            if (marker.dataset.id === currentSlide.toString()) marker.classList.add('active')
        })
    }
}

markers.forEach(marker => marker.addEventListener('click', e => {
    let target = e.target
    if (target.dataset.id <= slidesLength) {
        currentSlide = target.dataset.id
        changeSlide()
    }
}))