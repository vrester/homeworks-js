const togglePassword = document.querySelectorAll('.icon-password')
const form = document.querySelector('.password-form')
const input = document.querySelector('.password-input')
const inputRepeat = document.querySelector('.repeat')

togglePassword.forEach(element => element.addEventListener('click', e => {
    const target = e.target
    const inputArea = target.closest('label').querySelector('.password-input')
    // toggle type
    inputArea.type === 'password' ? inputArea.type = 'text' : inputArea.type = 'password'
    // toggle icon
    target.classList.toggle('fa-eye-slash');
}))

inputRepeat.onblur = function () {
    return input.value !== inputRepeat.value ? inputRepeat.classList.add('invalid') : false
}

inputRepeat.onfocus = function () {
    return this.classList.contains('invalid') ? inputRepeat.classList.remove('invalid') : false
}

form.onsubmit = function (e) {
    if (input.value.length === 0 || inputRepeat.value.length === 0) return alert('Заповніть форму')

    // Шукаю дів з класом result, якщо такого не находжу, то створюю такий дів
    let div = document.querySelector('.result')
    div ? div.remove() : div = document.createElement('div')
    div.classList.add('result')

    if (input.value !== inputRepeat.value) {
        e.preventDefault()
        div.style.color = 'red'
        div.textContent = 'Потрібно ввести однакові значення'
        return inputRepeat.after(div)
    } else {
        div.style.color = 'green'
        div.textContent = 'You are welcome!'
        return inputRepeat.after(div)
    }
}

