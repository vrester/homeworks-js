"use strict"

function getNumber() {
    let inputNumber = +prompt('Enter an integer greater than 1').trim()
    while (isNaN(inputNumber) || inputNumber < 1 || !Number.isInteger(inputNumber)) { // перевірка на введення цілого числа > 1
        alert('The number does not meet the conditions')
        inputNumber = +prompt('Enter an integer greater than 1', [inputNumber]).trim()
    }
    return inputNumber
}

function getFactorial(number) {
    if (number === 1) {
        return number
    }
    return number * getFactorial(number - 1);
}

alert(getFactorial(getNumber()));
