let m = null
let n = null
do {
    alert('Second number must be greater than first number')
    m = +prompt('Enter first positive number greater than 1')

    while (!Number.isInteger(m) || m <= 1) {
        alert('You entered wrong number')
        m = +prompt('Enter first positive number greater than 1', [m])
    }

    n = +prompt('Enter second positive number greater than 1')
    while (!Number.isInteger(n) || n <= 1) {
        n = +prompt('Enter second positive number greater than 1', [n])
    }
} while (m >= n)

primeNumber:
    for (let i = m; i <= n; i++) {

        for (let j = 2; j < i; j++) {
            if (i % j === 0) continue primeNumber
        }

        console.log(i)
    }

