let inputNumber = +prompt('Enter number');
let res = false;

while (!Number.isInteger(inputNumber)) { // цикл для перевірки чи введене число ціле
    inputNumber = +prompt('Enter integer number')
}
if (inputNumber < 0) {
    for (let i = -1; i >= inputNumber; i--) { // цикл для пошуку від'ємних чисел кратних 5
        if (i % 5 === 0) {
            console.log(i)
            res = true
        }
    }
} else {
    for (let i = 1; i <= inputNumber; i++) { // цикл для пошуку чисел кратних 5
        if (i % 5 === 0) {
            console.log(i)
            res = true
        }
    }
}
if (!res) { // перевірка чи були числа кратні 5
    console.log('Sorry, no numbers')
}
