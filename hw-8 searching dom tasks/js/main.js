// Знайти всі параграфи на сторінці та встановити колір фону #ff0000
let p = document.querySelectorAll('p')
p.forEach(paragraph => paragraph.style.backgroundColor = '#ff0000')

//Знайти елемент з id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль.
//Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
let list = document.querySelector('#optionsList')
console.log(list)

let listParent = list.parentElement
console.log(listParent)

if (list.hasChildNodes()) {
    let listChildNodes = list.childNodes
    listChildNodes.forEach(child => console.log(`nodeName - ${child.nodeName}, nodeType - ${child.nodeType}`))
}

//Встановіть в якості контенту елемента з класом testParagraph наступний параграф - This is a paragraph
document.querySelector('#testParagraph').innerText = 'This is a paragraph'

// Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
const element = document.querySelector('.main-header')
for (const child of element.children) {
    console.log(child)
    child.classList.add('nav-item')
}

//Варіант дати всім дочірнім елементам та їх дітям клас nav-item
// const getAllChildren = (element) => {
//     if (element.children.length === 0) return [element];
//
//     let allChildElements = [];
//
//     for (let i = 0; i < element.children.length; i++) {
//         let children = getAllChildren(element.children[i]);
//         if (children) allChildElements.push(...children);
//     }
//     allChildElements.push(element);
//
//     return allChildElements;
// };
//
// const elementChildrens = getAllChildren(element)
// console.log(elementChildrens)
// elementChildrens.forEach(element => element.classList.add('nav-item'))

//Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
let sectionTitle = document.querySelectorAll('.section-title')
sectionTitle.forEach(element => element.classList.remove('section-title'))


