const wrapper = document.querySelector('.wrapper')
const buttons = document.querySelectorAll('.btn')
document.addEventListener('click', e => {
    if (e.target.dataset.name === 'span') {
        e.target.remove()
    }
})

wrapper.addEventListener('click', e => {
    if (e.target.dataset.name === 'draw') {
        appendCircle('10px')
    } else if (e.target.dataset.name === 'submit') {
        let input = document.querySelector('.input')
        if (input.value) {
            appendCircle(`${input.value}px`)
        }
    } else if (e.target.dataset.name === 'draw-circle') {
        wrapper.append(createInput())
        buttons.forEach(btn => btn.remove())
        wrapper.append(createSubmitBtn())
    }
})

const createInput = () => {
    let input = document.createElement('input')
    input.classList.add('input')
    input.type = 'number'
    input.placeholder = 'Enter diameter of circle'
    return input
}

function createSubmitBtn() {
    let btn = document.createElement('btn')
    btn.textContent = 'Submit'
    btn.classList.add('btn')
    btn.dataset.name = 'submit'
    return btn
}

function createCircle(diameter = '10px') {
    // border 50% => diameter === radius
    let circle = document.createElement('span')
    circle.dataset.name = 'span'
    circle.style.display = 'inline-block'
    circle.style.width = diameter
    circle.style.height = diameter
    circle.style.borderRadius = '50%'
    circle.style.marginRight = '10px'
    // Якийсь баг, інколи не додає backgroundColor. Можливо проблема в формулі функції getRandomColor(Взяв з інету)
    while (!circle.style.backgroundColor) circle.style.backgroundColor = getRandomColor()
    return circle
}

function getRandomColor() {
    return `#${Math.floor(Math.random() * 16777215).toString(16)}`
}

function appendCircle(diameter) {
    wrapper.remove()
    for (let i = 0; i < 100; i++) {
        document.body.append(createCircle(diameter))
    }
}
