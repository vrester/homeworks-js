function getNumber() {
    let inputNumber = +prompt('Enter number').trim()
    while (isNaN(inputNumber)) { // перевірка на введення числа
        alert('You entered NaN. Try again')
        inputNumber = +prompt('Enter number', [inputNumber]).trim()
    }
    return inputNumber
}

const getMathOperationSign = () => {
    let result = prompt('Enter math operation (+, -, *, /)').trim()
    while (!isValid(result)) {
        result = prompt('You entered an incorrect mathematical operation sign. Enter one of the signs: +, -, *, /', [result]).trim()
    }

    function isValid(operation) { // перевірка на введення математичного знаку
        return !(operation !== '+' && operation !== '-' && operation !== '*' && operation !== '/');
    }

    return result
}

function getCalc(a, b, mathSign) {
    switch (mathSign) {
        case "+":
            return a + b
        case "-":
            return a - b
        case "*":
            return a * b
        case "/":
            return a / b
    }
}

const doMath = getCalc(getNumber(), getNumber(), getMathOperationSign())
console.log(doMath)
